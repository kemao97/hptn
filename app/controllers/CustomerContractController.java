package controllers;

import models.forms.CustomerContractFailureItemForm;
import models.forms.FailureItemForm;
import it.innove.play.pdf.PdfGenerator;
import models.*;
import play.Environment;
import play.data.Form;
import play.data.FormFactory;
import play.db.Database;
import play.mvc.BodyParser;
import play.mvc.Result;
import views.html.*;
import views.html.contract.*;

import javax.inject.Inject;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import static play.mvc.Results.*;

public class CustomerContractController {

    private Database db;
    private FormFactory formFactory;
    private PdfGenerator pdfGenerator;
    private Connection connection;

    @Inject
    public CustomerContractController(Database db, FormFactory formFactory, PdfGenerator pdfGenerator, Environment environment) {
        this.db = db;
        this.formFactory = formFactory;
        this.pdfGenerator = pdfGenerator;
        if(environment.isTest()) {
            this.connection = db.getConnection(false);
        } else {
            this.connection = db.getConnection();
        }
    }

    public CustomerContractController(Database db, FormFactory formFactory, PdfGenerator pdfGenerator, Connection connection) {
        this.db = db;
        this.formFactory = formFactory;
        this.pdfGenerator = pdfGenerator;
        this.connection = connection;
    }

    public Result home() {
        return ok(HomeView.render());
    }

    public Result showListCustomerContractUnpaid() {
        List<CustomerContract> customerContracts = getAllCustomerUnpaid();
        return ok(UnpaidListView.render(customerContracts));
    }

    public Result showCustomerContractUnpaid(int customerContractID) {
        CustomerContract customerContract = getCustomerContractUnpaid(customerContractID);
        if(customerContract == null) {
            return badRequest("Không tồn tại hợp đồng chưa thanh lý này");
        } else {
            int totalUnitPrice = calculateUnitPriceTotal(customerContract.getListCustomerContractCar());
            int dateRange = calculateDateRange(customerContract.getStartDate(), customerContract.getEndDate());
            int totalPrice = totalUnitPrice * dateRange;
            int restPrice = totalPrice - customerContract.getDepositMoney();
            return ok(UnpaidView.render(customerContract, totalUnitPrice, totalPrice, restPrice));
        }
    }

    public Result showCustomerContractPaid(int customerContractID) {
        PaidCustomerContract paidCustomerContract = getCustomerContractPaid(customerContractID);
        if(paidCustomerContract == null) {
            return badRequest("Không tồn tại hợp đồng đã thanh lý này");
        } else {
            CustomerContract customerContract = paidCustomerContract.getCustomerContract();
            java.util.Date signedDate = paidCustomerContract.getSignedDate();
            int totalUnitPrice = calculateUnitPriceTotal(customerContract.getListCustomerContractCar());
            int dateRange = calculateDateRange(customerContract.getStartDate(), customerContract.getEndDate());
            int totalPrice = totalUnitPrice * dateRange;
            int restPrice = totalPrice - customerContract.getDepositMoney();
            int failureMoney = 0;
            for (CustomerContractCar customerContractCar: customerContract.getListCustomerContractCar()) {
                for (FailureItem failureItem: customerContractCar.getListFailureItem()) {
                    failureMoney += failureItem.getCompensateMoney();
                }
            }
            int totalMoneyFinal = restPrice + failureMoney;
            return pdfGenerator.ok(PaidView.render(customerContract, totalUnitPrice, totalPrice, restPrice, failureMoney, totalMoneyFinal, signedDate), "");
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result payCustomerContract(int customerContractID) {
        Form<CustomerContractFailureItemForm> failureForm = formFactory.form(CustomerContractFailureItemForm.class);
        failureForm = failureForm.bindFromRequest();
        for (FailureItemForm failureItemForm: failureForm.get().getListFailureItemForm()) {
            CustomerContractCar customerContractCar = new CustomerContractCar();
            customerContractCar.setCustomerContractCarID(failureItemForm.getCustomerContractCarID());
            FailureItem failureItem = new FailureItem();
            failureItem.setDescription(failureItemForm.getDescription());
            failureItem.setCompensateMoney(failureItemForm.getCompensateMoney());
            failureItem.setCustomerContractCar(customerContractCar);

            addFailureItem(failureItem);
        }
        Staff staff = new Staff();
        staff.setStaffID(2);
        CustomerContract customerContract = new CustomerContract();
        customerContract.setCustomerContractID(customerContractID);
        PaidCustomerContract paidCustomerContract = new PaidCustomerContract();
        paidCustomerContract.setStaff(staff);
        paidCustomerContract.setCustomerContract(customerContract);
        paidCustomerContract.setSignedDate(new Timestamp(System.currentTimeMillis()));
        addPaidCustomerContract(paidCustomerContract);
        return ok("/contract/customer/paid/" + customerContractID);
    }

    /**
     * database function
     */

    public int addFailureItem(FailureItem failureItem) {
        int result = 0;
        String sql =
                "INSERT INTO `FailureItem` " +
                        "   (`description`, `compensateMoney`, `customerContractCarID`)\n" +
                        "VALUES (?, ?, ?);";
        String description = failureItem.getDescription();
        int compensateMoney = failureItem.getCompensateMoney();
        int customerContractCarID = failureItem.getCustomerContractCar().getCustomerContractCarID();
        try {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, description);
            statement.setInt(2, compensateMoney);
            statement.setInt(3, customerContractCarID);

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int addPaidCustomerContract(PaidCustomerContract paidCustomerContract) {
        int result = 0;
        String sql =
                "INSERT INTO `PaidCustomerContract` " +
                "   (`signedDate`, `staffID`, `customerContractID`)" +
                "VALUES (?, ?, ?)";
        Date signedDate = new Date(paidCustomerContract.getSignedDate().getTime());
        int staffID = paidCustomerContract.getStaff().getStaffID();
        int customerContractID = paidCustomerContract.getCustomerContract().getCustomerContractID();
        try {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setDate(1, signedDate);
            statement.setInt(2, staffID);
            statement.setInt(3, customerContractID);

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                result = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<CustomerContract> getAllCustomerUnpaid() {
        List<CustomerContract> result = new ArrayList<>();
        String sql =
                "SELECT contract.customerContractID, contract.startDate, contract.endDate, customer.`fullName` AS customerName \n" +
                "FROM \n" +
                "  `CustomerContract` contract \n" +
                "    JOIN `Customer` customer \n" +
                "      ON contract.`customerID` = customer.`customerID` \n" +
                "    LEFT JOIN `PaidCustomerContract` paid_contract \n" +
                "      ON contract.`customerContractID` = paid_contract.`customerContractID` \n" +
                "WHERE paid_contract.`paidCustomerContractID` IS NULL;";
        try {
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            while(rs.next()) {
                int customerContractID = rs.getInt("customerContractID");
                Date startDate = rs.getDate("startDate");
                Date endDate = rs.getDate("endDate");
                String customerName = rs.getString("customerName");

                Customer customer = new Customer();
                customer.setFullName(customerName);
                CustomerContract customerContract = new CustomerContract();
                customerContract.setCustomerContractID(customerContractID);
                customerContract.setStartDate(startDate);
                customerContract.setEndDate(endDate);
                customerContract.setCustomer(customer);

                result.add(customerContract);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public CustomerContract getCustomerContractUnpaid(int customerContractID) {
        CustomerContract result = null;
        String sql =
                "SELECT\n" +
                "  contract.*,\n" +
                "  customer.`fullName`,\n" +
                "  customer.`address`\n" +
                "FROM CustomerContract contract\n" +
                "  JOIN Customer customer\n" +
                "    ON contract.`customerID` = customer.`customerID`\n" +
                "  LEFT JOIN PaidCustomerContract paid_contract\n" +
                "    ON contract.customerContractID = paid_contract.customerContractID\n" +
                "WHERE paid_contract.paidCustomerContractID IS NULL\n" +
                "  AND contract.`customerContractID` = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerContractID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String fullName = rs.getString("fullName");
                String address = rs.getString("address");
                Date startDate = rs.getDate("startDate");
                Date endDate = rs.getDate("endDate");
                int depositMoney = rs.getInt("depositMoney");

                Customer customer = new Customer();
                customer.setFullName(fullName);
                customer.setAddress(address);
                List<CustomerContractCar> customerContractCars = getListCustomerContractCar(customerContractID);
                List<DepositItem> depositItems = getListDepositItem(customerContractID);
                CustomerContract customerContract = new CustomerContract();
                customerContract.setCustomerContractID(customerContractID);
                customerContract.setStartDate(startDate);
                customerContract.setEndDate(endDate);
                customerContract.setDepositMoney(depositMoney);
                customerContract.setCustomer(customer);
                customerContract.setListCustomerContractCar(customerContractCars);
                customerContract.setListDepositItem(depositItems);

                result = customerContract;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public PaidCustomerContract getCustomerContractPaid(int customerContractID) {
        PaidCustomerContract result = null;
        String sql =
                "SELECT\n" +
                "  contract.*,\n" +
                "  customer.`fullName`,\n" +
                "  customer.`address`,\n" +
                "  paid_contract.signedDate\n" +
                "FROM CustomerContract contract\n" +
                "  JOIN Customer customer\n" +
                "    ON contract.`customerID` = customer.`customerID`\n" +
                "  JOIN PaidCustomerContract paid_contract\n" +
                "    ON contract.customerContractID = paid_contract.customerContractID\n" +
                "WHERE contract.`customerContractID` = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerContractID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String fullName = rs.getString("fullName");
                String address = rs.getString("address");
                Date startDate = rs.getDate("startDate");
                Date endDate = rs.getDate("endDate");
                int depositMoney = rs.getInt("depositMoney");
                Date signedDate = rs.getDate("signedDate");

                Customer customer = new Customer();
                customer.setFullName(fullName);
                customer.setAddress(address);
                List<CustomerContractCar> customerContractCars = getListCustomerContractCar(customerContractID);
                List<DepositItem> depositItems = getListDepositItem(customerContractID);
                CustomerContract customerContract = new CustomerContract();
                customerContract.setCustomerContractID(customerContractID);
                customerContract.setStartDate(startDate);
                customerContract.setEndDate(endDate);
                customerContract.setDepositMoney(depositMoney);
                customerContract.setCustomer(customer);
                customerContract.setListCustomerContractCar(customerContractCars);
                customerContract.setListDepositItem(depositItems);
                PaidCustomerContract paidCustomerContract = new PaidCustomerContract();
                paidCustomerContract.setCustomerContract(customerContract);
                paidCustomerContract.setSignedDate(new Timestamp(signedDate.getTime()));

                result = paidCustomerContract;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<DepositItem> getListDepositItem(int customerContractID) {
        List<DepositItem> result = new ArrayList<>();
        String sql =
                "SELECT deposit_item.`description`\n" +
                "FROM CustomerContract contract\n" +
                " JOIN DepositItem deposit_item\n" +
                "   ON contract.`customerContractID` = deposit_item.`customerContractID`\n" +
                "WHERE contract.`customerContractID` = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerContractID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                String description = rs.getString("description");

                DepositItem depositItem = new DepositItem();
                depositItem.setDescription(description);
                result.add(depositItem);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public List<CustomerContractCar> getListCustomerContractCar(int customerContractID) {
        List<CustomerContractCar> result = new ArrayList<>();
        String sql =
                "SELECT contract_car.`customerContractCarID`\n" +
                "FROM CustomerContract contract\n" +
                " JOIN CustomerContractCar contract_car\n" +
                "   ON contract.`customerContractID` = contract_car.`customerContractID`\n" +
                "WHERE contract.`customerContractID` = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerContractID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int customerContractCarID = rs.getInt("customerContractCarID");
                CustomerContractCar customerContractCar = getCustomerContractCar(customerContractCarID);
                result.add(customerContractCar);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public CustomerContractCar getCustomerContractCar(int customerContractCarID) {
        CustomerContractCar result = getCustomerContractPartnerCar(customerContractCarID);
        if(result == null) {
            result = getCustomerContractStoreCar(customerContractCarID);
        }
        if(result != null) {
            List<FailureItem> failureItems = getListFailureItem(customerContractCarID);
            result.setListFailureItem(failureItems);
        }
        return result;
    }

    public List<FailureItem> getListFailureItem(int customerContractCarID) {
        List<FailureItem> result = new ArrayList<>();
        String sql =
                "SELECT failure.*\n" +
                "FROM CustomerContractCar contract_car\n" +
                " JOIN FailureItem failure\n" +
                "   ON contract_car.`customerContractCarID` = failure.`customerContractCarID`\n" +
                "WHERE contract_car.`customerContractCarID` = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerContractCarID);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                int failureItemID = rs.getInt("failureItemID");
                String description = rs.getString("description");
                int compensateMoney = rs.getInt("compensateMoney");

                FailureItem failureItem = new FailureItem();
                failureItem.setFailureItemID(failureItemID);
                failureItem.setDescription(description);
                failureItem.setCompensateMoney(compensateMoney);
                result.add(failureItem);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public CustomerContractPartnerCar getCustomerContractPartnerCar(int customerContractCarID) {
        CustomerContractPartnerCar result = null;
        String sql =
                "SELECT\n" +
                "  contract_car.*,\n" +
                "  car.*,\n" +
                "  brand_car.`brandName`,\n" +
                "  line_car.`lineCarName`\n" +
                "FROM CustomerContractCar contract_car\n" +
                "  JOIN `CustomerContractPartnerCar` contract_partner_car\n" +
                "    ON contract_car.`customerContractCarID` = contract_partner_car.`customerContractCarID`\n" +
                "  JOIN PartnerContract partner_contract\n" +
                "    ON contract_partner_car.`partnerContractID` = partner_contract.`partnerContractID`\n" +
                "  JOIN PartnerCar partner_car\n" +
                "    ON partner_contract.`partnerCarID` = partner_car.`partnerCarID`\n" +
                "  JOIN Car car\n" +
                "    ON partner_car.`carID` = car.`carID`\n" +
                "  JOIN BrandCar brand_car\n" +
                "    ON car.`brandCarID` = brand_car.`brandCarID`\n" +
                "  JOIN LineCar line_car\n" +
                "    ON car.`lineCarID` = line_car.`lineCarID`\n" +
                "WHERE contract_car.`customerContractCarID` = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerContractCarID);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                int carID = rs.getInt("carID");
                int unitPrice = rs.getInt("unitPrice");
                String license = rs.getString("license");
                String carName = rs.getString("carName");
                String version = rs.getString("version");
                String carDescription = rs.getString("description");
                String brandName = rs.getString("brandName");
                String lineCarName = rs.getString("lineCarName");
                String statusCar = rs.getString("statusCar");

                LineCar lineCar = new LineCar();
                lineCar.setLineCarName(lineCarName);
                BrandCar brandCar = new BrandCar();
                brandCar.setBrandCarName(brandName);
                PartnerCar partnerCar = new PartnerCar();
                partnerCar.setCarID(carID);
                partnerCar.setLicense(license);
                partnerCar.setCarName(carName);
                partnerCar.setVersion(version);
                partnerCar.setDescription(carDescription);
                partnerCar.setBrandCar(brandCar);
                partnerCar.setLineCar(lineCar);
                PartnerContract partnerContract = new PartnerContract();
                partnerContract.setPartnerCar(partnerCar);
                CustomerContractPartnerCar customerContractPartnerCar = new CustomerContractPartnerCar();
                customerContractPartnerCar.setPartnerContract(partnerContract);
                customerContractPartnerCar.setCustomerContractCarID(customerContractCarID);
                customerContractPartnerCar.setUnitPrice(unitPrice);
                customerContractPartnerCar.setStatusCar(statusCar);

                result = customerContractPartnerCar;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public CustomerContractStoreCar getCustomerContractStoreCar(int customerContractCarID) {
        CustomerContractStoreCar result = null;
        String sql =
                "SELECT\n" +
                "  contract_car.*,\n" +
                "  car.*,\n" +
                "  brand_car.`brandName`,\n" +
                "  line_car.`lineCarName`\n" +
                "FROM CustomerContractCar contract_car\n" +
                "  JOIN `CustomerContractStoreCar` contract_store_car\n" +
                "    ON contract_car.`customerContractCarID` = contract_store_car.`customerContractCarID`\n" +
                "  JOIN StoreCar store_car\n" +
                "    ON contract_store_car.`storeCarID` = store_car.`storeCarID`\n" +
                "  JOIN Car car\n" +
                "    ON store_car.`carID` = car.`carID`\n" +
                "  JOIN BrandCar brand_car\n" +
                "    ON car.`brandCarID` = brand_car.`brandCarID`\n" +
                "  JOIN LineCar line_car\n" +
                "    ON car.`lineCarID` = line_car.`lineCarID`\n" +
                "WHERE contract_car.`customerContractCarID` = ?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, customerContractCarID);
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                int carID = rs.getInt("carID");
                int unitPrice = rs.getInt("unitPrice");
                String license = rs.getString("license");
                String carName = rs.getString("carName");
                String version = rs.getString("version");
                String carDescription = rs.getString("description");
                String brandName = rs.getString("brandName");
                String lineCarName = rs.getString("lineCarName");
                String statusCar = rs.getString("statusCar");

                LineCar lineCar = new LineCar();
                lineCar.setLineCarName(lineCarName);
                BrandCar brandCar = new BrandCar();
                brandCar.setBrandCarName(brandName);
                StoreCar storeCar = new StoreCar();
                storeCar.setCarID(carID);
                storeCar.setLicense(license);
                storeCar.setCarName(carName);
                storeCar.setVersion(version);
                storeCar.setDescription(carDescription);
                storeCar.setBrandCar(brandCar);
                storeCar.setLineCar(lineCar);
                CustomerContractStoreCar customerContractStoreCar = new CustomerContractStoreCar();
                customerContractStoreCar.setStoreCar(storeCar);
                customerContractStoreCar.setCustomerContractCarID(customerContractCarID);
                customerContractStoreCar.setUnitPrice(unitPrice);
                customerContractStoreCar.setStatusCar(statusCar);

                result = customerContractStoreCar;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * utils function
     */

    public static String dateToString(java.util.Date date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormat.format(date);
    }

    public static Car getCar(CustomerContractCar customerContractCar) {
        if(customerContractCar == null) return null;
        if(customerContractCar instanceof CustomerContractPartnerCar) {
            return ((CustomerContractPartnerCar)customerContractCar).getPartnerContract().getPartnerCar();
        } else {
            return ((CustomerContractStoreCar)customerContractCar).getStoreCar();
        }
    }

    public static int calculateUnitPriceTotal(List<CustomerContractCar> customerContractCars) {
        int result = 0;
        for (CustomerContractCar customerContractCar: customerContractCars) {
            result += customerContractCar.getUnitPrice();
        }
        return result;
    }

    public static int calculateDateRange(java.util.Date startDate, java.util.Date endDate) {
        if(startDate.after(endDate)) {
            return 0;
        } else {
            return (int)((endDate.getTime() - startDate.getTime()) / (86400 * 1000));
        }
    }
}
