package models;

public class Customer extends User {

	private int customerID;

	public Customer() {
	}

	public int getCustomerID() {
		return this.customerID;
	}

	/**
	 * 
	 * @param customerID
	 */
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}

}