package models;

public class FailureItem {

	private int failureItemID;
	private String description;
	private int compensateMoney;
	private CustomerContractCar customerContractCar;

	public FailureItem() {
	}

	public int getFailureItemID() {
		return this.failureItemID;
	}

	/**
	 * 
	 * @param failureItemID
	 */
	public void setFailureItemID(int failureItemID) {
		this.failureItemID = failureItemID;
	}

	public String getDescription() {
		return this.description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public int getCompensateMoney() {
		return this.compensateMoney;
	}

	/**
	 * 
	 * @param compensateMoney
	 */
	public void setCompensateMoney(int compensateMoney) {
		this.compensateMoney = compensateMoney;
	}

	public CustomerContractCar getCustomerContractCar() {
		return this.customerContractCar;
	}

	/**
	 * 
	 * @param customerContractCar
	 */
	public void setCustomerContractCar(CustomerContractCar customerContractCar) {
		this.customerContractCar = customerContractCar;
	}

}