package models;

public class CustomerContractPartnerCar extends CustomerContractCar {

	private int customerContractPartnerCarID;
	private PartnerContract partnerContract;

	public CustomerContractPartnerCar() {
	}

	public int getCustomerContractPartnerCarID() {
		return this.customerContractPartnerCarID;
	}

	/**
	 * 
	 * @param customerContractPartnerCarID
	 */
	public void setCustomerContractPartnerCarID(int customerContractPartnerCarID) {
		this.customerContractPartnerCarID = customerContractPartnerCarID;
	}

	public PartnerContract getPartnerContract() {
		return this.partnerContract;
	}

	/**
	 * 
	 * @param partnerContract
	 */
	public void setPartnerContract(PartnerContract partnerContract) {
		this.partnerContract = partnerContract;
	}

}