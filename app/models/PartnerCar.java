package models;

public class PartnerCar extends Car {

	private int partnerCarID;
	private Partner partner;

	public PartnerCar() {
	}

	public int getPartnerCarID() {
		return this.partnerCarID;
	}

	/**
	 * 
	 * @param partnerCarID
	 */
	public void setPartnerCarID(int partnerCarID) {
		this.partnerCarID = partnerCarID;
	}

	public Partner getPartner() {
		return this.partner;
	}

	/**
	 * 
	 * @param partner
	 */
	public void setPartner(Partner partner) {
		this.partner = partner;
	}

}