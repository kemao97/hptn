package models;

public class Partner extends User {

	private int partnerID;

	public Partner() {
	}

	public int getPartnerID() {
		return this.partnerID;
	}

	/**
	 * 
	 * @param partnerID
	 */
	public void setPartnerID(int partnerID) {
		this.partnerID = partnerID;
	}

}