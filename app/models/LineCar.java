package models;

public class LineCar {

	private int lineCarID;
	private String lineCarName;

	public LineCar() {
	}

	public int getLineCarID() {
		return this.lineCarID;
	}

	/**
	 * 
	 * @param lineCarID
	 */
	public void setLineCarID(int lineCarID) {
		this.lineCarID = lineCarID;
	}

	public String getLineCarName() {
		return this.lineCarName;
	}

	/**
	 * 
	 * @param lineCarName
	 */
	public void setLineCarName(String lineCarName) {
		this.lineCarName = lineCarName;
	}

}