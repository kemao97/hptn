package models;

import java.sql.Timestamp;

public class PaidPartnerContract {

	private int paidPartnerContractID;
	private ManagerStaff managerStaff;
	private PartnerContract partnerContract;
	private Timestamp signedDate;

	public PaidPartnerContract() {
	}

	public int getPaidPartnerContractID() {
		return this.paidPartnerContractID;
	}

	/**
	 * 
	 * @param paidPartnerContractID
	 */
	public void setPaidPartnerContractID(int paidPartnerContractID) {
		this.paidPartnerContractID = paidPartnerContractID;
	}

	public ManagerStaff getManagerStaff() {
		return this.managerStaff;
	}

	/**
	 * 
	 * @param managerStaff
	 */
	public void setManagerStaff(ManagerStaff managerStaff) {
		this.managerStaff = managerStaff;
	}

	public PartnerContract getPartnerContract() {
		return this.partnerContract;
	}

	/**
	 * 
	 * @param partnerContract
	 */
	public void setPartnerContract(PartnerContract partnerContract) {
		this.partnerContract = partnerContract;
	}

	public Timestamp getSignedDate() {
		return this.signedDate;
	}

	/**
	 * 
	 * @param signedDate
	 */
	public void setSignedDate(Timestamp signedDate) {
		this.signedDate = signedDate;
	}

}