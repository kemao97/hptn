package models;

public class CustomerContractStoreCar extends CustomerContractCar {

	private int customerContractStoreCarID;
	private StoreCar storeCar;

	public CustomerContractStoreCar() {
	}

	public int getCustomerContractStoreCarID() {
		return this.customerContractStoreCarID;
	}

	/**
	 * 
	 * @param customerContractStoreCarID
	 */
	public void setCustomerContractStoreCarID(int customerContractStoreCarID) {
		this.customerContractStoreCarID = customerContractStoreCarID;
	}

	public StoreCar getStoreCar() {
		return this.storeCar;
	}

	/**
	 * 
	 * @param storeCar
	 */
	public void setStoreCar(StoreCar storeCar) {
		this.storeCar = storeCar;
	}

}