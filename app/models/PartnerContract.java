package models;

import java.util.Date;

public class PartnerContract {

	private int partnerContractID;
	private PartnerCar partnerCar;
	private ManagerStaff managerStaff;
	private Date startDate;
	private Date endDate;
	private String statusCar;
	private int unitPrice;

	public PartnerContract() {
	}

	public int getPartnerContractID() {
		return this.partnerContractID;
	}

	/**
	 * 
	 * @param partnerContractID
	 */
	public void setPartnerContractID(int partnerContractID) {
		this.partnerContractID = partnerContractID;
	}

	public PartnerCar getPartnerCar() {
		return this.partnerCar;
	}

	/**
	 * 
	 * @param partnerCar
	 */
	public void setPartnerCar(PartnerCar partnerCar) {
		this.partnerCar = partnerCar;
	}

	public ManagerStaff getManagerStaff() {
		return this.managerStaff;
	}

	/**
	 * 
	 * @param managerStaff
	 */
	public void setManagerStaff(ManagerStaff managerStaff) {
		this.managerStaff = managerStaff;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getStatusCar() {
		return this.statusCar;
	}

	/**
	 * 
	 * @param statusCar
	 */
	public void setStatusCar(String statusCar) {
		this.statusCar = statusCar;
	}

	public int getUnitPrice() {
		return this.unitPrice;
	}

	/**
	 * 
	 * @param unitPrice
	 */
	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}

}