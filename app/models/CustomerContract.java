package models;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class CustomerContract {

	private int customerContractID;
	private Customer customer;
	private Staff staff;
	private int depositMoney;
	private List<DepositItem> listDepositItem;
	private List<CustomerContractCar> listCustomerContractCar;
	private Date startDate;
	private Date endDate;
	private Timestamp createdAt;

	public CustomerContract() {
	}

	public int getCustomerContractID() {
		return this.customerContractID;
	}

	/**
	 * 
	 * @param customerContractID
	 */
	public void setCustomerContractID(int customerContractID) {
		this.customerContractID = customerContractID;
	}

	public Customer getCustomer() {
		return this.customer;
	}

	/**
	 * 
	 * @param customer
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public Staff getStaff() {
		return this.staff;
	}

	/**
	 * 
	 * @param staff
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public int getDepositMoney() {
		return this.depositMoney;
	}

	/**
	 * 
	 * @param depositMoney
	 */
	public void setDepositMoney(int depositMoney) {
		this.depositMoney = depositMoney;
	}

	public List<DepositItem> getListDepositItem() {
		return this.listDepositItem;
	}

	/**
	 * 
	 * @param listDepositItem
	 */
	public void setListDepositItem(List<DepositItem> listDepositItem) {
		this.listDepositItem = listDepositItem;
	}

	public List<CustomerContractCar> getListCustomerContractCar() {
		return this.listCustomerContractCar;
	}

	/**
	 * 
	 * @param listCustomerContractCar
	 */
	public void setListCustomerContractCar(List<CustomerContractCar> listCustomerContractCar) {
		this.listCustomerContractCar = listCustomerContractCar;
	}

	public Date getStartDate() {
		return this.startDate;
	}

	/**
	 * 
	 * @param startDate
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * 
	 * @param endDate
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	/**
	 * 
	 * @param createdAt
	 */
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

}