package models;

public class StoreCar extends Car {

	private int storeCarID;

	public StoreCar() {
	}

	public int getStoreCarID() {
		return this.storeCarID;
	}

	/**
	 * 
	 * @param storeCarID
	 */
	public void setStoreCarID(int storeCarID) {
		this.storeCarID = storeCarID;
	}

}