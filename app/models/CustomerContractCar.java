package models;

import java.util.List;

public class CustomerContractCar {

	private int customerContractCarID;
	private CustomerContract customerContract;
	private int unitPrice;
	private String statusCar;
	private List<FailureItem> listFailureItem;

	public CustomerContractCar() {
	}

	public int getCustomerContractCarID() {
		return this.customerContractCarID;
	}

	/**
	 * 
	 * @param customerContractCarID
	 */
	public void setCustomerContractCarID(int customerContractCarID) {
		this.customerContractCarID = customerContractCarID;
	}

	public CustomerContract getCustomerContract() {
		return this.customerContract;
	}

	/**
	 * 
	 * @param customerContract
	 */
	public void setCustomerContract(CustomerContract customerContract) {
		this.customerContract = customerContract;
	}

	public int getUnitPrice() {
		return this.unitPrice;
	}

	/**
	 * 
	 * @param unitPrice
	 */
	public void setUnitPrice(int unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getStatusCar() {
		return this.statusCar;
	}

	/**
	 * 
	 * @param statusCar
	 */
	public void setStatusCar(String statusCar) {
		this.statusCar = statusCar;
	}

	public List<FailureItem> getListFailureItem() {
		return this.listFailureItem;
	}

	/**
	 * 
	 * @param listFailureItem
	 */
	public void setListFailureItem(List<FailureItem> listFailureItem) {
		this.listFailureItem = listFailureItem;
	}

}