package models;

public class DepositItem {

	private int depositItemID;
	private String description;
	private CustomerContract customerContract;

	public DepositItem() {
	}

	public int getDepositItemID() {
		return this.depositItemID;
	}

	/**
	 * 
	 * @param depositItemID
	 */
	public void setDepositItemID(int depositItemID) {
		this.depositItemID = depositItemID;
	}

	public String getDescription() {
		return this.description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public CustomerContract getCustomerContract() {
		return this.customerContract;
	}

	/**
	 * 
	 * @param customerContract
	 */
	public void setCustomerContract(CustomerContract customerContract) {
		this.customerContract = customerContract;
	}

}