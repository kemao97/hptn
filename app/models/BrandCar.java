package models;

public class BrandCar {

	private int brandCarID;
	private String brandCarName;

	public BrandCar() {
	}

	public int getBrandCarID() {
		return this.brandCarID;
	}

	/**
	 * 
	 * @param brandCarID
	 */
	public void setBrandCarID(int brandCarID) {
		this.brandCarID = brandCarID;
	}

	public String getBrandCarName() {
		return this.brandCarName;
	}

	/**
	 * 
	 * @param brandCarName
	 */
	public void setBrandCarName(String brandCarName) {
		this.brandCarName = brandCarName;
	}

}