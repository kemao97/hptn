package models;

import java.sql.Timestamp;

public class Car {

	private int carID;
	private String license;
	private String carName;
	private BrandCar brandCar;
	private LineCar lineCar;
	private String version;
	private String description;
	private Timestamp createdAt;

	public Car() {
	}

	public int getCarID() {
		return this.carID;
	}

	/**
	 * 
	 * @param carID
	 */
	public void setCarID(int carID) {
		this.carID = carID;
	}

	public String getLicense() {
		return this.license;
	}

	/**
	 * 
	 * @param license
	 */
	public void setLicense(String license) {
		this.license = license;
	}

	public String getCarName() {
		return this.carName;
	}

	/**
	 * 
	 * @param carName
	 */
	public void setCarName(String carName) {
		this.carName = carName;
	}

	public BrandCar getBrandCar() {
		return this.brandCar;
	}

	/**
	 * 
	 * @param brandCar
	 */
	public void setBrandCar(BrandCar brandCar) {
		this.brandCar = brandCar;
	}

	public LineCar getLineCar() {
		return this.lineCar;
	}

	/**
	 * 
	 * @param lineCar
	 */
	public void setLineCar(LineCar lineCar) {
		this.lineCar = lineCar;
	}

	public String getVersion() {
		return this.version;
	}

	/**
	 * 
	 * @param version
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescription() {
		return this.description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	/**
	 * 
	 * @param createdAt
	 */
	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

}