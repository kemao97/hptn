package models;

import java.util.Date;

public class PaidCustomerContract {

	private int paidCustomerContractID;
	private CustomerContract customerContract;
	private Staff staff;
	private Date signedDate;

	public PaidCustomerContract() {
	}

	public int getPaidCustomerContractID() {
		return this.paidCustomerContractID;
	}

	/**
	 * 
	 * @param paidCustomerContractID
	 */
	public void setPaidCustomerContractID(int paidCustomerContractID) {
		this.paidCustomerContractID = paidCustomerContractID;
	}

	public CustomerContract getCustomerContract() {
		return this.customerContract;
	}

	/**
	 * 
	 * @param customerContract
	 */
	public void setCustomerContract(CustomerContract customerContract) {
		this.customerContract = customerContract;
	}

	public Staff getStaff() {
		return this.staff;
	}

	/**
	 * 
	 * @param staff
	 */
	public void setStaff(Staff staff) {
		this.staff = staff;
	}

	public Date getSignedDate() {
		return this.signedDate;
	}

	/**
	 * 
	 * @param signedDate
	 */
	public void setSignedDate(Date signedDate) {
		this.signedDate = signedDate;
	}

}