package models;

public class ManagerStaff extends Staff {

	private int managerStaffID;

	public ManagerStaff() {
	}

	public int getManagerStaffID() {
		return this.managerStaffID;
	}

	/**
	 * 
	 * @param managerStaffID
	 */
	public void setManagerStaffID(int managerStaffID) {
		this.managerStaffID = managerStaffID;
	}

}