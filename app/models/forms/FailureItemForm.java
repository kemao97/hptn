package models.forms;

public class FailureItemForm {

	private int customerContractCarID;
	private String description;
	private int compensateMoney;

	public FailureItemForm() {
	}

	public int getCustomerContractCarID() {
		return this.customerContractCarID;
	}

	/**
	 * 
	 * @param customerContractCarID
	 */
	public void setCustomerContractCarID(int customerContractCarID) {
		this.customerContractCarID = customerContractCarID;
	}

	public String getDescription() {
		return this.description;
	}

	/**
	 * 
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public int getCompensateMoney() {
		return this.compensateMoney;
	}

	/**
	 * 
	 * @param compensateMoney
	 */
	public void setCompensateMoney(int compensateMoney) {
		this.compensateMoney = compensateMoney;
	}

}