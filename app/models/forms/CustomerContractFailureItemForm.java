package models.forms;

import java.util.ArrayList;
import java.util.List;

public class CustomerContractFailureItemForm {

	private List<FailureItemForm> listFailureItemForm;

	public CustomerContractFailureItemForm() {
		this.listFailureItemForm = new ArrayList<>();
	}

	public List<FailureItemForm> getListFailureItemForm() {
		return this.listFailureItemForm;
	}

	/**
	 * 
	 * @param listFailureItemForm
	 */
	public void setListFailureItemForm(List<FailureItemForm> listFailureItemForm) {
		this.listFailureItemForm = listFailureItemForm;
	}

}