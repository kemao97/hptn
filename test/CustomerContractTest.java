import controllers.CustomerContractController;
import models.forms.CustomerContractFailureItemForm;
import models.forms.FailureItemForm;
import it.innove.play.pdf.PdfGenerator;
import models.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import play.Application;
import play.data.FormFactory;
import play.db.Database;
import java.sql.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import static play.test.Helpers.*;

import static org.junit.Assert.*;

public class CustomerContractTest {

    private Application application;
    private Database db;
    private FormFactory formFactory;
    private PdfGenerator pdfGenerator;
    private Connection connection;
    private CustomerContractController controller;

    @Before
    public void setup() {
        application = fakeApplication();
        db = application.injector().instanceOf(Database.class);
        formFactory = application.injector().instanceOf(FormFactory.class);
        pdfGenerator = application.injector().instanceOf(PdfGenerator.class);
        connection = db.getConnection(false);
        controller = new CustomerContractController(db, formFactory, pdfGenerator, connection);
        start(application);
    }

    @After
    public void clean() {
        try {
            connection.rollback();
            connection.close();
            db.shutdown();
            stop(application);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testHome() {
        Result result = controller.home();
        assertEquals(OK, result.status());
        assertEquals( "text/html", result.contentType().get());
        assertEquals("utf-8", result.charset().get());
        assertTrue(contentAsString(result).contains("Trang Chủ"));
    }

    @Test
    public void testShowListCustomerContractUnpaid() {
        Result result = controller.showListCustomerContractUnpaid();
        assertEquals(OK, result.status());
        assertEquals("text/html", result.contentType().get());
        assertEquals("utf-8", result.charset().get());
        assertTrue(contentAsString(result).contains("Danh sách các hợp đồng thuê xe chưa thanh lý"));
    }

    @Test
    public void testShowCustomerContractUnpaidValid() {
        int customerContractID = 6;
        Result result = controller.showCustomerContractUnpaid(customerContractID);
        assertEquals(OK, result.status());
        assertEquals("text/html", result.contentType().get());
        assertEquals("utf-8", result.charset().get());
        assertTrue(contentAsString(result).contains("HDT<span id=\"customerContractID\">6</span>"));
    }

    @Test
    public void testShowCustomerContractUnpaidInvalid() {
        int customerContractID = 2;
        Result result = controller.showCustomerContractUnpaid(customerContractID);
        assertEquals(BAD_REQUEST, result.status());
        assertEquals("text/plain", result.contentType().get());
        assertEquals("utf-8", result.charset().get());
        assertTrue(contentAsString(result).contains("Không tồn tại hợp đồng chưa thanh lý này"));
    }

    @Test
    public void testShowCustomerContractPaidValid() {
        String sql =
                "INSERT INTO `PaidCustomerContract` " +
                "   (`signedDate`, `staffID`, `customerContractID`) " +
                "VALUES (?, ?, ?)";
        java.sql.Date signedDate = new java.sql.Date(System.currentTimeMillis());
        int staffID = 2;
        int customerContractID = 6;
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setDate(1, signedDate);
            statement.setInt(2, staffID);
            statement.setInt(3, customerContractID);

            statement.execute();
            Result result = controller.showCustomerContractPaid(customerContractID);
            assertEquals(OK, result.status());
            assertEquals("application/pdf", result.contentType().get());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testShowCustomerContractPaidNotValid() {
        int customerContractID = 6;
        Result result = controller.showCustomerContractPaid(customerContractID);
        assertEquals(BAD_REQUEST, result.status());
        assertEquals("text/plain", result.contentType().get());
        assertTrue(contentAsString(result).contains("Không tồn tại hợp đồng đã thanh lý này"));
    }

    @Test
    public void testPayCustomerContractHaveFailure() {
        CustomerContractFailureItemForm customerContractFailureItemForm = new CustomerContractFailureItemForm();
        Http.RequestBuilder requestBuilder = fakeRequest("POST", "/contract/customer/6")
                .bodyJson(Json.toJson(customerContractFailureItemForm));
        Result result = route(requestBuilder);
        assertEquals(OK, result.status());
        assertEquals("text/plain", result.contentType().get());
        assertEquals("/contract/customer/paid/6", contentAsString(result));
    }

    @Test
    public void testPayCustomerContractNotFailure() {
        List<FailureItemForm> failureItemForms = new ArrayList<>();
        FailureItemForm failureItemForm = new FailureItemForm();
        failureItemForm.setCustomerContractCarID(1);
        failureItemForm.setDescription("Chết máy");
        failureItemForm.setCompensateMoney(100000);
        failureItemForms.add(failureItemForm);
        CustomerContractFailureItemForm customerContractFailureItemForm = new CustomerContractFailureItemForm();
        customerContractFailureItemForm.setListFailureItemForm(failureItemForms);
        Http.RequestBuilder requestBuilder = fakeRequest("POST", "/contract/customer/6")
                .bodyJson(Json.toJson(customerContractFailureItemForm));
        Result result = route(requestBuilder);
        assertEquals(OK, result.status());
        assertEquals("text/plain", result.contentType().get());
        assertEquals("/contract/customer/paid/6", contentAsString(result));
    }

    /**
     * database testing
     */

    @Test
    public void testAddFailureItemValid() {
        CustomerContractCar customerContractCar = new CustomerContractCar();
        customerContractCar.setCustomerContractCarID(1);
        FailureItem failureItem = new FailureItem();
        failureItem.setCustomerContractCar(customerContractCar);
        failureItem.setDescription("hello world");
        failureItem.setCompensateMoney(100000);
        int failureID = controller.addFailureItem(failureItem);
        assertNotEquals(0, failureID);

        String sql = "select * from FailureItem where failureItemID = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, failureID);
            ResultSet rs = statement.executeQuery();
            assertTrue(rs.next());
            assertEquals(failureItem.getDescription(), rs.getString("description"));
            assertEquals(failureItem.getCompensateMoney(), rs.getInt("compensateMoney"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddFailureItemInvalid() {
        CustomerContractCar customerContractCar = new CustomerContractCar();
        customerContractCar.setCustomerContractCarID(1);
        FailureItem failureItem = new FailureItem();
        failureItem.setCustomerContractCar(customerContractCar);
        failureItem.setCompensateMoney(100000);
        int failureID = controller.addFailureItem(failureItem);
        assertEquals(0, failureID);

        String sql = "SELECT * FROM FailureItem WHERE failureItemID = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, failureID);
            ResultSet rs = statement.executeQuery();
            assertFalse(rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddPaidCustomerContractValid() {
        Staff staff = new Staff();
        staff.setStaffID(2);
        CustomerContract customerContract = new CustomerContract();
        customerContract.setCustomerContractID(6);
        PaidCustomerContract paidCustomerContract = new PaidCustomerContract();
        paidCustomerContract.setStaff(staff);
        paidCustomerContract.setCustomerContract(customerContract);
        paidCustomerContract.setSignedDate(new Date(System.currentTimeMillis()));
        int paidCustomerContractID = controller.addPaidCustomerContract(paidCustomerContract);
        assertNotEquals(0, paidCustomerContractID);

        String sql = "SELECT * FROM PaidCustomerContract WHERE paidCustomerContractID = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, paidCustomerContractID);
            ResultSet rs = statement.executeQuery();
            assertTrue(rs.next());
            assertEquals(paidCustomerContract.getStaff().getStaffID(), rs.getInt("staffID"));
            assertEquals(paidCustomerContract.getCustomerContract().getCustomerContractID(), rs.getInt("customerContractID"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAddPaidCustomerContractInvalid() {
        Staff staff = new Staff();
        staff.setStaffID(1);
        CustomerContract customerContract = new CustomerContract();
        customerContract.setCustomerContractID(6);
        PaidCustomerContract paidCustomerContract = new PaidCustomerContract();
        paidCustomerContract.setStaff(staff);
        paidCustomerContract.setCustomerContract(customerContract);
        paidCustomerContract.setSignedDate(new Date(System.currentTimeMillis()));
        int paidCustomerContractID = controller.addPaidCustomerContract(paidCustomerContract);
        assertEquals(0, paidCustomerContractID);

        String sql = "SELECT * FROM PaidCustomerContract WHERE paidCustomerContractID = ?;";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, paidCustomerContractID);
            ResultSet rs = statement.executeQuery();
            assertFalse(rs.next());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetAllCustomerNotPaidNonEmpty() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        List<CustomerContract> customerContracts = controller.getAllCustomerUnpaid();
        assertEquals(3, customerContracts.size());
        CustomerContract contract = null;
        for (CustomerContract customerContract: customerContracts) {
            if (customerContract.getCustomerContractID() == 6) {
                contract = customerContract;
            }
        }
        assertNotNull(contract);
        try {
            assertEquals(sdf.parse("01-01-2019"), contract.getStartDate());
            assertEquals(sdf.parse("03-01-2019"), contract.getEndDate());
            assertEquals("Nguyễn Duy Định", contract.getCustomer().getFullName());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetAllCustomerNotPaidEmpty() {
        String sql =
                "INSERT INTO `PaidCustomerContract` " +
                "   (`signedDate`, `staffID`, `customerContractID`) " +
                "VALUES (?, ?, ?)";
        java.sql.Date signedDate = new java.sql.Date(System.currentTimeMillis());
        int staffID = 2;
        int[] customerContractIDs = {6, 8, 10};
        for (int customerContractID: customerContractIDs) {
            try {
                PreparedStatement statement = connection.prepareStatement(sql);
                statement.setDate(1, signedDate);
                statement.setInt(2, staffID);
                statement.setInt(3, customerContractID);

                statement.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        List<CustomerContract> customerContracts = controller.getAllCustomerUnpaid();
        assertEquals(0, customerContracts.size());
    }

    @Test
    public void testGetCustomerContractUnpaidNotNull() {
        CustomerContract contract = controller.getCustomerContractUnpaid(6);
        assertNotNull(contract);

        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            assertEquals(sdf.parse("01-01-2019"), contract.getStartDate());
            assertEquals(sdf.parse("03-01-2019"), contract.getEndDate());
            assertEquals("Nguyễn Duy Định", contract.getCustomer().getFullName());
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetCustomerContractUnpaidNull() {
        CustomerContract contract = controller.getCustomerContractUnpaid(1);
        assertNull(contract);
    }

    @Test
    public void testGetCustomerContractPaidNotNull() {
        String sql =
                "INSERT INTO `PaidCustomerContract` " +
                "   (`signedDate`, `staffID`, `customerContractID`) " +
                "VALUES (?, ?, ?)";
        java.sql.Date signedDate = new java.sql.Date(System.currentTimeMillis());
        int staffID = 2;
        int customerContractID = 6;
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setDate(1, signedDate);
            statement.setInt(2, staffID);
            statement.setInt(3, customerContractID);

            statement.execute();
            PaidCustomerContract contract = controller.getCustomerContractPaid(6);
            assertNotNull(contract);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetCustomerContractPaidNull() {
        PaidCustomerContract contract = controller.getCustomerContractPaid(1);
        assertNull(contract);
    }

    @Test
    public void testGetListDepositItemNonEmpty() {
        List<DepositItem> depositItems = controller.getListDepositItem(6);
        assertNotEquals(0, depositItems.size());
    }

    @Test
    public void testGetListDepositItemEmpty() {
        List<DepositItem> depositItems = controller.getListDepositItem(2);
        assertEquals(0, depositItems.size());
    }

    @Test
    public void testGetListCustomerContractCarNotEmpty() {
        List<CustomerContractCar> contractCars = controller.getListCustomerContractCar(6);
        assertNotEquals(0, contractCars.size());
    }

    @Test
    public void testGetListCustomerContractCarEmpty() {
        List<CustomerContractCar> contractCars = controller.getListCustomerContractCar(1);
        assertEquals(0, contractCars.size());
    }

    @Test
    public void testGetCustomerContractCarIsPartnerCar() {
        CustomerContractCar contractCar = controller.getCustomerContractCar(1);
        assertTrue(contractCar instanceof CustomerContractPartnerCar);
    }

    @Test
    public void testGetCustomerContractCarIsStoreCar() {
        CustomerContractCar contractCar = controller.getCustomerContractCar(3);
        assertTrue(contractCar instanceof CustomerContractStoreCar);
    }

    @Test
    public void testGetCustomerContractCarNull() {
        CustomerContractCar contractCar = controller.getCustomerContractCar(5);
        assertNull(contractCar);
    }

    @Test
    public void testGetListFailureItemNonEmpty() {
        String sql =
                "INSERT INTO `FailureItem` " +
                "   (`description`, `compensateMoney`, `customerContractCarID`)\n" +
                "VALUES (?, ?, ?);";
        String description = "description";
        int compensateMoney = 100000;
        int customerContractCarID = 4;
        try {
            PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, description);
            statement.setInt(2, compensateMoney);
            statement.setInt(3, customerContractCarID);

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            int failureID = 0;
            if (rs.next()) {
                failureID = rs.getInt(1);
            }
            List<FailureItem> failureItems = controller.getListFailureItem(customerContractCarID);
            assertNotEquals(0, failureItems.size());
            boolean isExistFailureItem = false;
            for (FailureItem failureItem: failureItems) {
                if(failureItem.getFailureItemID() == failureID) {
                    isExistFailureItem = true;
                }
            }
            assertTrue(isExistFailureItem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetListFailureItemEmpty() {
        List<FailureItem> failureItems = controller.getListFailureItem(1);
        assertEquals(0, failureItems.size());
    }

    @Test
    public void testGetCustomerContractPartnerCarNotNull() {
        CustomerContractPartnerCar contractPartnerCar = controller.getCustomerContractPartnerCar(1);
        assertNotNull(contractPartnerCar);
    }

    @Test
    public void testGetCustomerContractPartnerCarNull() {
        CustomerContractPartnerCar contractPartnerCar = controller.getCustomerContractPartnerCar(3);
        assertNull(contractPartnerCar);
    }

    @Test
    public void testGetCustomerContractStoreCarNotNull() {
        CustomerContractStoreCar contractStoreCar = controller.getCustomerContractStoreCar(4);
        assertNotNull(contractStoreCar);
    }

    @Test
    public void testGetCustomerContractStoreCarNull() {
        CustomerContractStoreCar contractStoreCar = controller.getCustomerContractStoreCar(1);
        assertNull(contractStoreCar);
    }

    /**
     * utils testing
     */

    @Test
    public void testDateToString() {
        Date date = new Date(0);
        assertEquals("01/01/1970", controller.dateToString(date));
    }

    @Test
    public void testGetCarFromPartnerCar() {
        int carID = 100;
        PartnerCar partnerCar = new PartnerCar();
        partnerCar.setCarID(100);
        PartnerContract partnerContract = new PartnerContract();
        partnerContract.setPartnerCar(partnerCar);
        CustomerContractPartnerCar customerContractPartnerCar = new CustomerContractPartnerCar();
        customerContractPartnerCar.setPartnerContract(partnerContract);
        CustomerContractCar contractCar = (CustomerContractCar) customerContractPartnerCar;

        Car car = controller.getCar(contractCar);
        assertEquals(carID, car.getCarID());
    }

    @Test
    public void testGetCarFromStoreCar() {
        int carID = 100;
        StoreCar storeCar = new StoreCar();
        storeCar.setCarID(carID);
        CustomerContractStoreCar customerContractStoreCar = new CustomerContractStoreCar();
        customerContractStoreCar.setStoreCar(storeCar);
        CustomerContractCar contractCar = (CustomerContractStoreCar) customerContractStoreCar;

        Car car = controller.getCar(contractCar);
        assertEquals(carID, car.getCarID());
    }

    @Test
    public void testGetCarNull() {
        CustomerContractCar contractCar = null;
        Car car = controller.getCar(contractCar);
        assertNull(car);
    }

    @Test
    public void testCalculateUnitPriceTotalNonZero() {
        int firstPrice = 100000;
        int secondPrice = 200000;
        CustomerContractCar first = new CustomerContractCar();
        first.setUnitPrice(firstPrice);
        CustomerContractCar second = new CustomerContractCar();
        second.setUnitPrice(secondPrice);
        List<CustomerContractCar> customerContractCars = new ArrayList<>();
        customerContractCars.add(first);
        customerContractCars.add(second);
        int totalCalculate = controller.calculateUnitPriceTotal(customerContractCars);
        assertEquals(firstPrice + secondPrice, totalCalculate);
    }

    @Test
    public void testCalculateUnitPriceTotalZero() {
        List<CustomerContractCar> customerContractCars = new ArrayList<>();
        int totalCalculate = controller.calculateUnitPriceTotal(customerContractCars);
        assertEquals(0, totalCalculate);
    }

    @Test
    public void testCalculateDateRangeValid() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date startDate = sdf.parse("01-03-2019");
            Date endDate = sdf.parse("03-03-2019");
            assertEquals(2, controller.calculateDateRange(startDate, endDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCalculateDateRangeInvalid() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date startDate = sdf.parse("05-03-2019");
            Date endDate = sdf.parse("03-03-2019");
            assertEquals(0, controller.calculateDateRange(startDate, endDate));
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
