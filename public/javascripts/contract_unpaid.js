$(document).ready(function () {
    var failureIDIncrementJs = 1;
    var failureArr = [];
    var totalMoney = parseInt($("#totalMoney").text());

    $('#addFailure').click(function () {
        var contractCarID = $("#failureForm #contractCarID").val();
        var contractCarValue = $("#failureForm #contractCarID").find("option:selected").text();
        var description = $("#failureForm #description").val();
        var compensateMoney = $("#failureForm #compensateMoney").val();

        if(validateFailure(contractCarID, description, compensateMoney)) {
            var failure = {};
            failure.failureIDJs = failureIDIncrementJs++;
            failure.contractCarID = contractCarID;
            failure.contractCarValue = contractCarValue;
            failure.description = description;
            failure.compensateMoney = parseInt(compensateMoney);
            failureArr.splice(failureArr.length - 1, 0, failure);

            updateFailureTable();
            updateCompensateMoney();
            clearFailureForm();
        }
    });

    $(document).on("click", ".failure-remove", function () {
        if(confirm("Bạn có chắc chắn muốn xoá?")) {
            var row = $(this).parents("tr");
            var failureID = parseInt(row.find(".failureID").text());

            for(var i = 0; i < failureArr.length; i++) {
                if(failureArr[i].failureIDJs === failureID) {
                    failureArr.splice(i, 1);
                    break;
                }
            }
            updateFailureTable();
            updateCompensateMoney();
        }
    });

    $("#contractSubmit").click(function () {
        var customerContractID = $("#customerContractID").text();
        var failureData = {};
        failureData.listFailureItemForm = [];
        for(var i = 0; i < failureArr.length; i++) {
            var failure = {};
            failure.customerContractCarID = failureArr[i].contractCarID;
            failure.description = failureArr[i].description;
            failure.compensateMoney = failureArr[i].compensateMoney;
            failureData.listFailureItemForm.push(failure);
        }
        $.ajax({
            type: 'POST',
            url: '/contract/customer/' + customerContractID,
            dataType: 'text',
            contentType: 'application/json',
            processData: false,
            data: JSON.stringify(failureData),
            success: function(data, textStatus, jqXHR) {
                alert("Thanh lý thành công!!");
                location.href = data;
            },
            error: function (jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    });

    function updateFailureTable() {
        var container = $("#failureTable tbody");
        container.empty();
        if(failureArr.length === 0) {
            var row =
                "<tr>\n" +
                "    <td class=\"text-center\" colspan=\"4\">Không có hỏng hóc</td>\n" +
                "</tr>";
            container.html(row);
        }
        failureArr.forEach(function (value) {
            var failureIDJs = value.failureIDJs;
            var contractCarValue = value.contractCarValue;
            var description = value.description;
            var compensateMoney = value.compensateMoney;

            var row =
                "<tr>\n" +
                "   <td class='failureID' style='display: none;'>" + failureIDJs + "</td> \n" +
                "   <td>" + contractCarValue +"</td>\n" +
                "   <td>" + description +"</td>\n" +
                "   <td class='text-right'>" + compensateMoney +"</td>\n" +
                "   <td class='text-center'>\n" +
                "       <button type=\"button\" class=\"btn btn-danger failure-remove\">Xoá</button>\n" +
                "   </td>\n" +
                "</tr>";
            container.prepend(row);
        });
    }

    function updateCompensateMoney() {
        var totalCompensateMoney = 0;
        failureArr.forEach(function (value) {
            totalCompensateMoney += value.compensateMoney;
        });
        $("#failureTable #compensateTotalMoney").html(totalCompensateMoney);
        $("#totalMoneyFinal").html(totalMoney + totalCompensateMoney)
        if(totalCompensateMoney === 0) {
            $("#failureTable #compensateTotalMoneyRow").hide();
        } else {
            $("#failureTable #compensateTotalMoneyRow").show();
        }
    }

    function clearFailureForm() {
        $("#failureForm #contractCarID").val("");
        $("#failureForm #description").val("");
        $("#failureForm #compensateMoney").val("");
    }

    function validateFailure(contractCarID, description, compensateMoney) {
        if(contractCarID === "") {
            alert("Yêu cầu chọn xe");
            return false;
        } else if(description === "") {
            alert("Mô tả không được để trống");
            return false;
        } else if(compensateMoney === "") {
            alert("Đền bù không được để trống");
            return false;
        } else if(!parseInt(compensateMoney) && compensateMoney !== "0") {
            alert("Đền bù yêu cầu nhập số");
            return false;
        } else if(parseInt(compensateMoney) < 0) {
            alert("Đền bù yêu cầu nhập số không âm");
            return false;
        } else if(parseInt(compensateMoney) > 1000000000) {
            alert("Đền bù không nhập quá 1 tỷ");
            return false;
        }
        return true;
    }
});